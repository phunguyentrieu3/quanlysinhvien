package QLSinhVien.QLSinhvien.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.util.Set;

@Data
@Entity(name = "MonHoc")
@Table(name = "MonHoc")
public class MonHoc {
    @Id
    @Column(name = "MaMon", length = 10)
    @Size(min = 1, max = 10, message = "Khong duoc de trong")
    private String maMon;

    @Size(min = 5, max = 50, message = "ten mon hoc phai tu 5 den 50 ky tu")
    @Column(name = "TenMonHoc", length = 50)
    private String tenMonHoc;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "SinhVien_MonHoc",
            joinColumns = {@JoinColumn(name = "MaMon")},
            inverseJoinColumns = {@JoinColumn(name = "MSSV")}
    )
    private Set<SinhVien> sinhViens;
}
