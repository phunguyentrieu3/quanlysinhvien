package QLSinhVien.QLSinhvien.controller;

import QLSinhVien.QLSinhvien.entity.Lop;
import QLSinhVien.QLSinhvien.entity.MonHoc;
import QLSinhVien.QLSinhvien.entity.SinhVien;
import QLSinhVien.QLSinhvien.services.LopService;
import QLSinhVien.QLSinhvien.services.SinhVienService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/sinhvien")
public class SinhVienController {

    @Autowired
    private SinhVienService sinhVienService;

    @Autowired
    private LopService lopService;

    @GetMapping
    public String showAllSinhVien(Model model) {
        List<SinhVien> dsSinhVien = sinhVienService.getAllSinhVien();
        model.addAttribute("dsSinhVien", dsSinhVien);
        return "sinhvien/list";
    }

    @GetMapping("/add")
    public String showAddForm(Model model) {
        model.addAttribute("sinhvien", new SinhVien());
        List<Lop> dsLop = lopService.getAllLop();
        model.addAttribute("dsLop", dsLop);
        return "sinhvien/add";
    }

    @PostMapping("/add")
    public String addSinhVien(@ModelAttribute("sinhvien") SinhVien sinhVien) {
        Lop selectedLop = lopService.getLopById(sinhVien.getLop().getMaLop());
        sinhVien.setLop(selectedLop);
        sinhVienService.addSinhVien(sinhVien);
        return "redirect:/sinhvien";
    }
    @GetMapping("/edit/{id}")
    public String showEditForm(@PathVariable("id") String id, Model model) {
        SinhVien sinhVien = sinhVienService.getSinhVienById(id);
        if (sinhVien != null) {
            model.addAttribute("sinhVien", sinhVien);
            return "sinhVien/edit";
        }
        return "redirect:/sinhVien";
    }
    @PostMapping("/edit/{id}")
    public String updateLop(@PathVariable("id") String id, @ModelAttribute("sinhVien") SinhVien sinhVienDetails){
        SinhVien sinhVien = sinhVienService.getSinhVienById(id);
        if(sinhVien != null){
            sinhVien.setMssv(sinhVienDetails.getMssv());
            sinhVienService.updateSinhVien(sinhVien);
        }
        return "redirect:/sinhVien";
    }
    @GetMapping("/delete/{id}")
    public String deleteLop(@PathVariable("id") String id){
        sinhVienService.deleteSinhVien(id);
        return "redirect:/sinhvien";
    }
}
