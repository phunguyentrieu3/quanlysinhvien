package QLSinhVien.QLSinhvien.controller;

import QLSinhVien.QLSinhvien.entity.Lop;
import QLSinhVien.QLSinhvien.entity.MonHoc;
import QLSinhVien.QLSinhvien.services.MonHocService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/monhoc")
public class MonHocController {
    @Autowired
    private MonHocService monHocService;

    @GetMapping
    public String ShowAllMonHoc(Model model) {
        List<MonHoc> dsMonHoc = monHocService.getAllmonHoc();
        model.addAttribute("dsMonHoc", dsMonHoc);
        return "monhoc/list";
    }
    @GetMapping("/add")
    public String showAddForm(Model model) {
        model.addAttribute("monhoc", new MonHoc());
        return "monhoc/add";
    }
    @PostMapping("/add")
    public String addLop(@ModelAttribute("monhoc") MonHoc monHoc) {
        monHocService.addMonHoc(monHoc);
        return "redirect:/monhoc";
    }
    @GetMapping("/edit/{id}")
    public String showEditForm(@PathVariable("id") String id, Model model) {
        MonHoc monHoc = monHocService.getmonHocByID(id);
        if (monHoc != null) {
            model.addAttribute("monhoc", monHoc);
            return "monhoc/edit";
        }
        return "redirect:/monhoc";
    }
    @PostMapping("/edit/{id}")
    public String updateLop(@PathVariable("id") String id, @ModelAttribute("monhoc") MonHoc monHocDetails){
        MonHoc monHoc = monHocService.getmonHocByID(id);
        if(monHoc != null){
            monHoc.setTenMonHoc(monHocDetails.getTenMonHoc());
            monHocService.updatedMonHoc(monHoc);
        }
        return "redirect:/monhoc";
    }
    @GetMapping("/delete/{id}")
    public String deleteLop(@PathVariable("id") String id){
        monHocService.deleteMonHoc(id);
        return "redirect:/monhoc";
    }
}
