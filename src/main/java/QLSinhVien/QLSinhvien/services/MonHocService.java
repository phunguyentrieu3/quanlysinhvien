package QLSinhVien.QLSinhvien.services;

import QLSinhVien.QLSinhvien.entity.MonHoc;
import QLSinhVien.QLSinhvien.reposioty.IMonHocRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MonHocService {
    @Autowired
    private IMonHocRepository monHocRepository;

    public List<MonHoc> getAllmonHoc(){
        return monHocRepository.findAll();
    }
    public MonHoc getmonHocByID(String id){
        return monHocRepository.findById(id).orElse(null);
    }
    public void addMonHoc(MonHoc monHoc){
        monHocRepository.save(monHoc);
    }
    public void deleteMonHoc(String id){
        monHocRepository.deleteById(id);
    }
    public void updatedMonHoc(MonHoc monHoc){
        monHocRepository.save(monHoc);
    }
}
