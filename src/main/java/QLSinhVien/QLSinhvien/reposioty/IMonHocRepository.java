package QLSinhVien.QLSinhvien.reposioty;

import QLSinhVien.QLSinhvien.entity.MonHoc;
import QLSinhVien.QLSinhvien.entity.SinhVien;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IMonHocRepository extends JpaRepository<MonHoc, String> {
}
